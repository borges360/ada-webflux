# ada-webflux

## Grupo
- Luiz Felipe Cruz Borges
- Adriano
- Alex Monteiro Ramos
- Francisco Washington De Almeida Oliveira
- Leonardo Forner Locatti

## Motivação

Existem alguns motivos para utilizar Spring Webflux em um projeto de API web de cadastro de livros, entre eles:

Alta escalabilidade: Spring Webflux é construído em cima do modelo de programação reativa, que permite lidar com um grande número de solicitações simultâneas sem a necessidade de aumentar a quantidade de recursos do sistema.

Menor consumo de recursos: Ao contrário do modelo de programação tradicional, que utiliza uma thread por solicitação, o modelo reativo do Spring Webflux é capaz de lidar com várias solicitações com uma quantidade menor de threads, o que resulta em um menor consumo de recursos do sistema.

Melhor desempenho: O modelo reativo também permite que as operações de entrada e saída sejam executadas de forma assíncrona, o que pode melhorar significativamente o desempenho da aplicação.

Suporte a streaming: Spring Webflux suporta streaming, o que significa que é possível transmitir dados em tempo real para os clientes, sem a necessidade de esperar até que todos os dados estejam prontos para serem enviados.

Suporte a vários protocolos: Spring Webflux suporta vários protocolos de rede, incluindo HTTP/1.1, HTTP/2 e WebSocket, o que permite que a aplicação se adapte a diferentes cenários de uso.

Facilidade de desenvolvimento: Spring Webflux possui uma sintaxe simples e fácil de entender, o que torna o desenvolvimento da aplicação mais fácil e rápido.

Maior resiliência: O modelo reativo permite que a aplicação lide melhor com falhas e erros, tornando-a mais resistente a falhas e mais fácil de recuperar de erros.

## Recurso Spring Cloud Utilizado

Projeto API Gateway: https://github.com/washolv/Gateway

O Spring Cloud API Gateway é um componente do Spring Cloud que permite a criação de gateways de API que podem ser usados para rotear e controlar o tráfego de várias APIs de forma centralizada.

Em uma aplicação web de cadastro de livros, o uso do Spring Cloud API Gateway pode trazer vários benefícios, como:

Roteamento de tráfego: O Spring Cloud API Gateway permite rotear o tráfego de várias APIs em um único ponto de entrada, facilitando a gestão e manutenção das APIs.

Controle de acesso: O API Gateway pode ser usado para controlar o acesso às APIs, definindo políticas de autenticação e autorização.

Implementação de políticas de segurança: O Gateway pode ser configurado para implementar políticas de segurança, como a proteção contra ataques DDoS, bloqueio de IPs suspeitos, entre outras.

Monitoramento do tráfego: O Gateway pode ser configurado para monitorar o tráfego de cada API, permitindo identificar gargalos e pontos de melhoria na performance.

Implementação de balanceamento de carga: O Gateway pode ser configurado para implementar balanceamento de carga entre várias instâncias de uma API, melhorando a disponibilidade e performance da aplicação.

Configuração centralizada: O Spring Cloud API Gateway permite a configuração centralizada das APIs, tornando mais fácil a gestão e manutenção da aplicação.

Além desses benefícios, o uso do Spring Cloud API Gateway também pode trazer maior flexibilidade e escalabilidade para a aplicação, permitindo a inclusão de novas APIs de forma mais fácil e rápida.
